extends Control
export (int) var Index
export (int) var Points
export (bool) var Enable=false
signal Select(Index)
signal Buy(Index)
var Checked=false

func _ready():
	_Price(Points)

func _setChecked(value):
	Checked=value
	if(value):
		$Button.modulate.a=255
	else:
		$Button.modulate.a=0

func EnableClick(value):
	if(value):
		$Button/Button.call_deferred("show")
	else:
		$Button/Button.call_deferred("hide")

func _setEnable(value):
	Enable=value

func _Price(value):
	if(Enable || value==0):
		Enable=true
		$Price.text=""
	elif(!Enable && value>-1):
		$Price.text=str(value)
	elif(value==-1):
		$Price.text="30th Daily Bonus!"

func _Buy():
	emit_signal("Select",Index)

func _on_Button_pressed():
	if(Enable):
		emit_signal("Select",Index)
	else:
		emit_signal("Buy",Index)
