extends PathFollow2D
export (int) var Index
export var Speed=8
export var rotatio=1
var bppos = 0

#MOB SPECIAL
var newIndex=-1
func _ready():
	applaySpeed()

func applaySpeed():
		Speed=Speed*10

func _physics_process(delta):
	self.set_offset(self.get_offset()+(Speed*delta))
	$Mob.rotate(rotatio*delta)

func destoryed():
	$TimerChange.stop()
	$AnimationPlayer.stop()
	$Dead.play()
	rotatio=0
	Speed=0
	$Mob/CollisionShape2D.disabled=true
	$Mob/Sprite.play("destroyed")

func set_TimeChangeWait(wait):
	$TimerChange.wait_time=wait

func start_TimeChange():
	if($Mob.is_in_group("MobDown")):
		$TimerChange.start()

func _on_TimerChange_timeout():
	if($Mob.is_in_group("MobDown")):
		bppos=0
		if(Index>0):
			$AnimationPlayer.play( "changeSfera")

func _on_Sprite_animation_finished():
	if($Mob/Sprite.animation=="destroyed"):
		self.queue_free()

func _on_AnimationPlayer_animation_finished(anim_name):
	if($Mob.is_in_group("MobDown") && $Mob/Sprite.animation!="destroyed"):
		get_tree().get_nodes_in_group("Main")[0]._on_ChangeSfera_body_entered($Mob)
		$AnimationPlayer.stop()
