extends Control
export (int) var Index
signal Select(Index)

func _setChecked(value):
	if(value):
		$Control.modulate.a=255
	else:
		$Control.modulate.a=0

func _on_Button_pressed():
		emit_signal("Select",Index)


