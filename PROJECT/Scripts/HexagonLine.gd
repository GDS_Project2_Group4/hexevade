extends Node
export (PackedScene) var Mob
export (PackedScene) var MobDown
export (PackedScene) var MobSpecial
export (Array) var PowerUps
export (PackedScene) var Bomb
export var PlayerSpeed=17
export var PointPackman=3
export var PointBomb=1
export var MobSpeed=17
export var MobTime=2
export var MobStart=5
export var MobMax=20
export var MobDownTime=10
export (int) var MobDownPerSocer=10
export (int) var MobSpecialPerSocer=30
export var PowerUpsMinTime=13
export var PowerUpsMaxTime=18
export var PowerUpsMinSpeed=40
export var PowerUpsMaxSpeed=40
export var PowerIsAngel=true
export var TimeSpeedPowerUps=5
export var TimeEatPowerUps=5
export var ProcentSpeedDegressMob=0.35
export var ScaleBombEfect=1
export (int) var Respawn=1
#MENU
export (int) var MapVariant=1
export (int) var PlayerVariant=1
export (int) var ControlVariant=1
export (int) var VibrateVariant=0
export (int) var Sound=60
export (int) var Music=60

var Points=0
var BestScore=0
var score
var orginalMobsSpeed
var SferaArray
var MobSferaArray
var MobHexagonSferaArray
var isInstanceMobDown=false
var isInstanceMobSpecial=false
var AngleArray
var Player
var efectsPowerUps="empty"

func _ready():
	get_tree().paused=true
	get_tree().set_auto_accept_quit(false)
	
	SferaArray=self.get_tree().get_nodes_in_group("Sfera")
	MobSferaArray=self.get_tree().get_nodes_in_group("MobSfera")
	MobHexagonSferaArray=self.get_tree().get_nodes_in_group("MobSferaHexagon")
	AngleArray=self.get_tree().get_nodes_in_group("Angle")
	randomize()
	load_config()
	setProperty()
	Player=self.get_tree().get_nodes_in_group("Player")[0]
	randomStartMobs(MobStart)
	$HUD/Play/Intro.play()

func randomStartMobs(count):
	for i in range(0,count):
		var randomIndex=int(rand_range(0,MobSferaArray.size()-1))
		var mob = Mob.instance()
		mob.Speed=MobSpeed
		MobSferaArray[randomIndex].add_child(mob)
		mob.set_offset(rand_range(0,MobSferaArray[randomIndex].curve.get_baked_length()))

func mobInstance():
		var mobsSize=get_tree().get_nodes_in_group("Mob").size()
		var mobsDownSize=get_tree().get_nodes_in_group("MobDown").size()
		var mobsSpecialSize=get_tree().get_nodes_in_group("MobSpecial").size()
		if(mobsSize-mobsDownSize-mobsSpecialSize>=MobMax):
			return
		var mob
		if(isInstanceMobDown && mobsDownSize<2):
			isInstanceMobDown=false
			mob = MobDown.instance()
			mob.set_TimeChangeWait(MobDownTime-3)
		elif(isInstanceMobSpecial && mobsSpecialSize<1):
			isInstanceMobSpecial=false
			mob = MobSpecial.instance()
		else:
			mob = Mob.instance()
		var randomIndex=int(rand_range(0,MobSferaArray.size()))
		mob.Speed=MobSpeed
		MobSferaArray[randomIndex].add_child(mob)

func _on_Play_pressed():
		$HUD/Play.hide()
		$HUD/Play/Intro.stop()
		if(ControlVariant==1):
			$HUD/HUD/ScrollRight.call_deferred("show")
		elif(ControlVariant==2):
			$HUD/HUD/ScrollLeft.call_deferred("show")
		elif(ControlVariant==3):
			$HUD/HUD/upRight.call_deferred("show")
			$HUD/HUD/downRight.call_deferred("show")
		elif(ControlVariant==4):
			$HUD/HUD/upLeft.call_deferred("show")
			$HUD/HUD/downLeft.call_deferred("show")
		new_game()

func PrintBestScore():
	$HUD/Play/Play/BestScore.text=str(BestScore)

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        get_tree().change_scene("res://Scenes/Menu.tscn")
    elif what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        get_tree().change_scene("res://Scenes/Menu.tscn")

func game_over():
    $MobTimer.stop()
    $PowerTimer.stop()
    $HUD.show_game_over();
    Points=Points+score
    if(BestScore<score):
    	BestScore=score
    save_config()
    if(ControlVariant==1):
    	$HUD/HUD/ScrollRightDetect.call_deferred("hide")
    elif(ControlVariant==2):
    	$HUD/HUD/ScrollLeftDetect.call_deferred("hide")
    elif(ControlVariant==3):
    	$HUD/HUD/upRight.call_deferred("hide")
    	$HUD/HUD/downRight.call_deferred("hide")
    elif(ControlVariant==4):
    	$HUD/HUD/upLeft.call_deferred("hide")
    	$HUD/HUD/downLeft.call_deferred("hide")
    get_tree().paused=true

func new_game():
    get_tree().paused=false
    score = 0
    Player.start()
    $StartTimer.start()
    $Main.play()
    $HUD.update_score(score)
    $Sfera/SfersAndPoints/CenterArea/AnimatedSprite.play("default")

func _on_HUD_respawn():
	Respawn=$HUD.IsRespawn
	save_config()
	get_tree().paused=false
	_on_Bomb_pressed()
	Player.respown()
	$StartTimer.start()
	$HUD.update_score(score)
	if(ControlVariant==1):
	    	$HUD/HUD/ScrollRightDetect.call_deferred("show")
	elif(ControlVariant==2):
	    	$HUD/HUD/ScrollLeftDetect.call_deferred("show")
	elif(ControlVariant==3):
	    	$HUD/HUD/upRight.call_deferred("show")
	    	$HUD/HUD/downRight.call_deferred("show")
	elif(ControlVariant==4):
	    	$HUD/HUD/upLeft.call_deferred("show")
	    	$HUD/HUD/downLeft.call_deferred("show")

func _on_StartTimer_timeout():
    $MobTimer.start()
    $PowerTimer.start()


func _on_Scroll_value_changed(value):
	$HUD/HUD/ScrollRight.value=value
	var Index=Player.get_parent().Index
	var newIndex=SferaArray.size()-value
	_changeSfera(newIndex)

func _on_ScrollLeft_value_changed(value):
	$HUD/HUD/ScrollLeft.value=value
	var Index=Player.get_parent().Index
	var newIndex=SferaArray.size()-value
	_changeSfera(newIndex)

func _changeSfera(Index):
	SferaArray[Index].unit_offset=SferaArray[Player.get_parent().Index].unit_offset
	Player.get_parent().remove_child(Player)
	SferaArray[Index].add_child(Player)

func AddSocer(var count):
	if(count==0):
		return
	for point in range(1,count+1):
		if((score+point)%MobDownPerSocer==0):
			isInstanceMobDown=true
		
		if((score+point)%MobSpecialPerSocer==0):
			isInstanceMobSpecial=true
	score += count
	$HUD.update_score(score)


func _input(event):
	var up =  event.is_action("ui_up") and event.is_pressed()
	var down =  event.is_action("ui_down") and event.is_pressed()
	var cancel =  event.is_action("ui_cancel") and event.is_pressed()
	
	if(cancel):
		get_tree().change_scene("res://Scenes/Menu.tscn")
	
	var Index=Player.get_parent().Index
	if(up  && Index<SferaArray.size()-1):
		 _changeSfera(Index+1)
	elif (down  && Index>0):
		 _changeSfera(Index-1)

func _on_MobTimer_timeout():
		mobInstance()

func _on_CenterArea_body_entered(body):
	if(body.is_in_group("Mob")):
		body.get_parent().destoryed()
		AddSocer(1)

#POWER UPS###########################################################
func _on_Player_bombPowerUps(body):
		_on_Bomb_pressed()
		body.queue_free()

func _on_Player_eatPowerUps(body):
		_on_Packman_pressed()
		body.queue_free()

func _on_Player_speedPowerUps(body):
		body.queue_free()
		_on_Speed_pressed()

func _on_PowerTimer_timeout():
    var time=rand_range(PowerUpsMinTime, PowerUpsMaxTime)
    var power = PowerUps[randi()%PowerUps.size()].instance()
    power.min_speed=PowerUpsMinSpeed
    power.max_speed=PowerUpsMaxSpeed
    var pulsAndMinus=[-1,1]
    $Sfera.add_child(power)
    power.global_position = $Sfera/SfersAndPoints/CenterPosition.global_position
    if(PowerIsAngel):
    	#ANGLE
    	var targetVector = (power.global_position - AngleArray[randi()%AngleArray.size()].global_position).normalized()
    	power.moveVector=targetVector
    	power.speed=rand_range(power.min_speed, power.max_speed)
    else:
    	power.moveVector=Vector2(pulsAndMinus[randi()%pulsAndMinus.size()]* rand_range(0.01, 1.0), pulsAndMinus[randi()%pulsAndMinus.size()]*rand_range(0.01, 1.0))
    	power.speed=rand_range(power.min_speed, power.max_speed)
    $PowerTimer.wait_time=time

func _on_EfectSpeedPowerUpsTimer_timeout():
	MobSpeed=orginalMobsSpeed
	var mobs=get_tree().get_nodes_in_group("Mob")
	for mob in mobs:
		mob.get_parent().Speed=orginalMobsSpeed
		mob.get_parent().applaySpeed()
	$MobTimer.start()

func _on_EfectEatPowerUpsTimer_timeout():
	var player=self.get_tree().get_nodes_in_group("Player")[0]
	if(player.isEatState()):
		player.eatDefaultState()

func _on_Speed_pressed():
	$MobTimer.stop()
	MobSpeed=MobSpeed*ProcentSpeedDegressMob
	var mobs=get_tree().get_nodes_in_group("Mob")
	for mob in mobs:
		mob.get_parent().Speed=MobSpeed
		mob.get_parent().applaySpeed()
	$EfectSpeedPowerUpsTimer.wait_time=TimeSpeedPowerUps
	$EfectSpeedPowerUpsTimer.start()


func _on_Bomb_pressed():
	var bomb = Bomb.instance()
	bomb.pointBomb=PointBomb
	bomb.scale=Vector2(ScaleBombEfect,ScaleBombEfect)
	$Sfera/SfersAndPoints.add_child(bomb)
	bomb.global_position = self.get_tree().get_nodes_in_group("Player")[0].global_position
	bomb.startBum()

func _on_Packman_pressed():
	Player.eatState()
	$EfectEatPowerUpsTimer.wait_time=TimeEatPowerUps-1
	$EfectEatPowerUpsTimer.start()
##########################################################################

#FOR DISIGNERS############################################################
func setProperty():
	
	#PLAYER
	var player
	if(PlayerVariant==1):
		player=preload("res://Scenes/PlayerHexagon.tscn").instance()
		player.connect("hit", self, "game_over")
		player.connect("speedPowerUps", self, "_on_Player_speedPowerUps")
		player.connect("bombPowerUps", self, "_on_Player_bombPowerUps")
		player.connect("eatPowerUps", self, "_on_Player_eatPowerUps")
		$Sfera/SfersAndPoints/Sfera5/PathFollow2D.add_child(player)
	elif(PlayerVariant==2):
		player=preload("res://Scenes/PlayerHexagon2.tscn").instance()
		player.connect("hit", self, "game_over")
		player.connect("speedPowerUps", self, "_on_Player_speedPowerUps")
		player.connect("bombPowerUps", self, "_on_Player_bombPowerUps")
		player.connect("eatPowerUps", self, "_on_Player_eatPowerUps")
		$Sfera/SfersAndPoints/Sfera5/PathFollow2D.add_child(player)
	elif(PlayerVariant==3):
		player=preload("res://Scenes/PlayerHexagon3.tscn").instance()
		player.connect("hit", self, "game_over")
		player.connect("speedPowerUps", self, "_on_Player_speedPowerUps")
		player.connect("bombPowerUps", self, "_on_Player_bombPowerUps")
		player.connect("eatPowerUps", self, "_on_Player_eatPowerUps")
		$Sfera/SfersAndPoints/Sfera5/PathFollow2D.add_child(player)
	elif(PlayerVariant==4):
		player=preload("res://Scenes/PlayerHexagon4.tscn").instance()
		player.connect("hit", self, "game_over")
		player.connect("speedPowerUps", self, "_on_Player_speedPowerUps")
		player.connect("bombPowerUps", self, "_on_Player_bombPowerUps")
		player.connect("eatPowerUps", self, "_on_Player_eatPowerUps")
		$Sfera/SfersAndPoints/Sfera5/PathFollow2D.add_child(player)
	elif(PlayerVariant==5):
		player=preload("res://Scenes/PlayerHexagon5.tscn").instance()
		player.connect("hit", self, "game_over")
		player.connect("speedPowerUps", self, "_on_Player_speedPowerUps")
		player.connect("bombPowerUps", self, "_on_Player_bombPowerUps")
		player.connect("eatPowerUps", self, "_on_Player_eatPowerUps")
		$Sfera/SfersAndPoints/Sfera5/PathFollow2D.add_child(player)
	player.pointPackman = PointPackman
	for sfer in SferaArray:
		sfer.Speed=PlayerSpeed
		sfer.applaySpeed()
		sfer.set_offset (150)

	#RESPAWN
	$HUD.IsRespawn=Respawn
	
	#POWER UPS
	$PowerTimer.wait_time=rand_range(PowerUpsMinTime, PowerUpsMaxTime)
	#MOB
	$MobTimer.wait_time=MobTime
	orginalMobsSpeed=MobSpeed
	
	#MAP
	if(MapVariant==1):
		$Sfera.texture=preload("res://Assets/Mapa/Mapa1.png")
	elif(MapVariant==2):
		$Sfera.texture=preload("res://Assets/Mapa/Mapa2.png")
	elif(MapVariant==3):
		$Sfera.texture=preload("res://Assets/Mapa/Mapa3.png")
	elif(MapVariant==4):
		$Sfera.texture=preload("res://Assets/Mapa/Mapa5.png")
	elif(MapVariant==5):
		$Sfera.texture=preload("res://Assets/Mapa/Mapa_4.png")
	
	#CONTROL
	if(ControlVariant==1):
		$HUD/Play/Play.texture=preload("res://Assets/PLAY Button/PLAY BUTTON 1.png")
		$HUD/HUD/upLeft.queue_free()
		$HUD/HUD/downLeft.queue_free()
		$HUD/HUD/upRight.queue_free()
		$HUD/HUD/downRight.queue_free()
		$HUD/HUD/ScrollLeft.queue_free()
		$HUD/HUD/ScrollLeftDetect.queue_free()
	elif(ControlVariant==2):
		$HUD/Play/Play.texture=preload("res://Assets/PLAY Button/PLAY_BUTTON_4.png")
		$HUD/HUD/upLeft.queue_free()
		$HUD/HUD/downLeft.queue_free()
		$HUD/HUD/upRight.queue_free()
		$HUD/HUD/downRight.queue_free()
		$HUD/HUD/ScrollRight.queue_free()
		$HUD/HUD/ScrollRightDetect.queue_free()
	elif(ControlVariant==3):
		$HUD/Play/Play.texture=preload("res://Assets/PLAY Button/PLAY BUTTON 2.png")
		$HUD/HUD/ScrollLeft.queue_free()
		$HUD/HUD/ScrollLeftDetect.queue_free()
		$HUD/HUD/ScrollRight.queue_free()
		$HUD/HUD/ScrollRightDetect.queue_free()
		$HUD/HUD/upLeft.queue_free()
		$HUD/HUD/downLeft.queue_free()
	elif(ControlVariant==4):
		$HUD/Play/Play.texture=preload("res://Assets/PLAY Button/PLAY_BUTTON_3.png")
		$HUD/HUD/ScrollLeft.queue_free()
		$HUD/HUD/ScrollLeftDetect.queue_free()
		$HUD/HUD/ScrollRight.queue_free()
		$HUD/HUD/ScrollRightDetect.queue_free()
		$HUD/HUD/upRight.queue_free()
		$HUD/HUD/downRight.queue_free()
	
	#AUDIO
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),-60+Music)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"),-60+Sound)
	
	#POINT
	PrintBestScore()

func save_config(): 
	var path = "user://points.ini" # The path to save the file
	var config = ConfigFile.new() # Create a new ConfigFile object 
	config.set_value("points", "value", Points)
	config.set_value("player", "respawn", Respawn)
	config.set_value("points", "best", BestScore)
	var err = config.save(path) # Save the configuration file to the disk 
	if err != OK: # If there's an error 
		print("Some error occurred")

func load_config(): 
	var path = "user://config.ini" # The path to load the file 
	var config = ConfigFile.new() # Create a new ConfigFile object 
	var err = config.load(path) 
	# Load the file from the disk 
	if err == OK:  
		##MENU
		var sound=int(config.get_value("audio", "sound", Sound))
		if(sound>=0):
			Sound = sound
		
		var music=int(config.get_value("audio", "music", Music))
		if(music>=0):
			Music = music
		
		var vibrateVariant=int(config.get_value("audio", "vibrateVariant", VibrateVariant))
		if(vibrateVariant>=0):
			VibrateVariant = vibrateVariant
		
		var controlVariant=int(config.get_value("control", "variant", ControlVariant))
		if(controlVariant>0):
			ControlVariant = controlVariant
		
		var mapVariant=int(config.get_value("mapa", "variant", MapVariant))
		if(mapVariant>0):
			MapVariant = mapVariant
		
		var playerVariant=int(config.get_value("player", "variant", PlayerVariant))
		if(playerVariant>0):
			PlayerVariant = playerVariant

	path = "user://points.ini" # The path to load the file 
	config = ConfigFile.new() # Create a new ConfigFile object 
	err = config.load(path) 
	# Load the file from the disk 
	if err == OK:
		#Points
		var points=int(config.get_value("points", "value", Points))
		if(points>0):
			Points = points
		
		var bestScore=int(config.get_value("points", "best", BestScore))
		if(bestScore>0):
			BestScore = bestScore
		
		#RESPAWN
		var respawn=float(config.get_value("player", "respawn", Respawn))
		if(respawn>=0):
			Respawn = respawn
###########################################################################################

func ChangeSferaMob(mob,newIndex):
		var new_offset=float(0)
		var distans=-1
		var unit=mob.unit_offset
		var newMob=mob
		for i in range(0,MobHexagonSferaArray[newIndex].curve.get_baked_points().size()):
			if(distans==-1 || distans>MobHexagonSferaArray[newIndex].curve.get_baked_points()[i].distance_to(mob.position)):#.get_parent().curve.interpolate_baked(mob.offset))):
				new_offset=float(i)
				distans=MobHexagonSferaArray[newIndex].curve.get_baked_points()[i].distance_to(mob.position)#mob.get_parent().curve.interpolate_baked(mob.offset))
		new_offset=float(float((MobHexagonSferaArray[newIndex].curve.get_baked_length())*float(new_offset))/float(MobHexagonSferaArray[newIndex].curve.get_baked_points().size()))
		mob.get_parent().call_deferred("remove_child",mob)
		MobHexagonSferaArray[newIndex].call_deferred("add_child",newMob)
		newMob.Index=newIndex
		newMob.call_deferred("set_offset" ,(float(new_offset+5)))
		newMob.start_TimeChange()
		return

func _on_ChangeSfera_body_entered(body):
	if(body.get_parent().bppos==0 && body.get_parent().Index>0):
		body.get_parent().bppos=1
		var mob=body.get_parent()
		var index=mob.Index
		ChangeSferaMob(mob,index-1)

func _on_SpecialChangeSfera_body_entered(body,newIndex):
	if(body.get_parent().bppos==0):
		body.get_parent().bppos=1
		var mob=body.get_parent()
		var index=mob.Index
		ChangeSferaMob(mob,newIndex)

func _on_ChangeExit_body_entered(body):
	if(body.get_parent().bppos==0):
		body.get_parent().bppos=1
		var mob=body.get_parent()
		mob.get_parent().remove_child(mob)
		$Sfera/SfersAndPoints/MobSferaExit.add_child(mob)
		mob.unit_offset =1

func _on_ChangeExit2_body_entered(body):
	if(body.get_parent().bppos==0):
		body.get_parent().bppos=1
		var mob=body.get_parent()
		mob.get_parent().remove_child(mob)
		$Sfera/SfersAndPoints/MobSferaExit2.add_child(mob)
		mob.unit_offset =1

###MOBSPECIAL#########################################
func _on_UpChangeSfera_ChangeSfera(body):
		var mob=body.call("get_parent")
		mob.bppos=2
		var index=mob.Index
		ChangeSferaMob(mob,index-1)
		#mob.bppos=1
		return

func _on_UpChangeSfera_upChangeSfera(up, body):
		var mob=body.call("get_parent")
		mob.bppos=3
		mob.call("get_parent").call_deferred ("remove_child",body.call("get_parent"))
		up.call_deferred("add_child",mob)
		mob.unit_offset=0
		mob.bppos=3
		return

func _on_DownChangeSfera_ChangeSfera(body):
		var mob=body.call("get_parent")
		mob.bppos=2
		var index=mob.Index
		ChangeSferaMob(mob,index+1)
		#mob.bppos=1
		return

func _on_DownChangeSfera_DownChangeSfera(down, body):
	var mob=body.call("get_parent")
	mob.bppos=3
	mob.call("get_parent").call_deferred ("remove_child",body.call("get_parent"))
	down.call_deferred("add_child",mob)
	mob.unit_offset=0
	mob.bppos=3
	return
###########################################################

