extends Control
signal respawn

export (int) var TimeRespawn=5
export (int) var IsRespawn=1
var time
func _ready():
	time=TimeRespawn
	$HUD/Buttons/Respawn/RespawnTime.text=str(time)
	$HUD/Buttons.call_deferred("hide")

func show_game_over():
	if(IsRespawn>0):
		$HUD/TimerRespawn.call_deferred("start")
		$HUD/Buttons/Respawn.call_deferred("show")
	else:
		$HUD/Buttons/Respawn.call_deferred("hide")
	$HUD/Buttons/Respowns/Respawn.text=str(IsRespawn)
	$HUD/Buttons.call_deferred("show")
	$HUD/Score.call_deferred("hide")

func update_score(score):
    $HUD/Score/ScoreLabel.text = str(score)
    $HUD/Buttons/ScoreLabel.text = str(score)

func _on_Main_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")

func _on_Play_pressed():
	get_tree().change_scene("res://Scenes/HexagonLine.tscn")

func _on_Respawn_pressed():
	IsRespawn=IsRespawn-1
	$HUD/Score.call_deferred("show")
	$HUD/TimerRespawn.call_deferred("stop")
	$HUD/Buttons/Respawn.call_deferred("hide")
	$HUD/Buttons.call_deferred("hide")
	emit_signal("respawn")

func _on_TimerRespawn_timeout():
	if(time>1):
		time=time-1
		$HUD/Buttons/Respawn/RespawnTime.text=str(time)
	else:
		$HUD/TimerRespawn.call_deferred("stop")
		$HUD/Buttons/Respawn.call_deferred("hide")

func _on_Help_pressed():
	$HUD/Buttons/Respawn/RespawnTime.pause_mode= Node.PAUSE_MODE_INHERIT
	$HUD/Buttons.call_deferred("hide")
	$HUD/How.call_deferred("show")

func _on_Back_pressed():
	$HUD/Buttons/Respawn/RespawnTime.pause_mode=Node.PAUSE_MODE_PROCESS
	$HUD/Buttons.call_deferred("show")
	$HUD/How.call_deferred("hide")
